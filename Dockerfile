ARG UBUNTU_VERSION=20.04
ARG PYTHON_VERSION=3.8
ARG CUDA=11.8

FROM nvidia/cuda${ARCH:+-$ARCH}:${CUDA}.0-cudnn8-runtime-ubuntu${UBUNTU_VERSION}

SHELL ["/bin/bash", "-c"]

ENV TZ=Europe/Paris LANG=C.UTF-8 LC_ALL=C.UTF-8 DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    curl \
    ca-certificates \
    libjpeg-dev \
    libpng-dev \
    git && \
    apt -y dist-upgrade && \
    apt clean && \
    rm -fr /var/lib/apt/lists/*


# Install conda and python environment
ARG MINICONDA_ARCH=x86_64
ARG CONDA_PREFIX=/opt/conda
RUN curl -fsSL -v -o ~/miniconda.sh -O  "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-${MINICONDA_ARCH}.sh"
RUN chmod +x ~/miniconda.sh && \
    ~/miniconda.sh -b -p /opt/conda && \
    rm ~/miniconda.sh 

ENV PATH /opt/conda/bin:$PATH

# quick fix to specify python version
ENV PYTHON_VERSION 3.8

# install dependency for GPU Tensorflow
RUN  conda install -y -c conda-forge python=${PYTHON_VERSION} cmake ncurses conda-build pyyaml cudatoolkit=11.2 cudnn=8.1.0

# chose specific python version as default
RUN ln -s /opt/conda/bin/python${PYTHON_VERSION} /usr/local/bin/python

# update default python packages
RUN python -m pip install --upgrade pip setuptools requests

# install TensorRT (Optional) to improve latency and throughput for inference.
# RUN python -m pip install --upgrade nvidia-index
RUN python -m pip install --upgrade nvidia-tensorrt


# install needed package
COPY requirements.txt .
RUN python -m pip install --no-cache-dir -r requirements.txt

ENV LD_LIBRARY_PATH /opt/conda/lib:$LD_LIBRARY_PATH
ENV PATH /opt/conda/bin:$PATH

# link tensorflow cuda libninfer library to the correct place
RUN mkdir -p /usr/local/nvidia/lib 
RUN ln -s /opt/conda/lib/python${PYTHON_VERSION}/site-packages/tensorrt/libnvinfer.so.8 /usr/local/nvidia/lib/libnvinfer.so.7
RUN ln -s /opt/conda/lib/python${PYTHON_VERSION}/site-packages/tensorrt/libnvinfer_plugin.so.8 /usr/local/nvidia/lib/libnvinfer_plugin.so.7


# fetch Jupyter configuration from FIDLE gitlab
RUN mkdir -p ~/.jupyter/nbconfig
RUN curl -fsSL -v -o ~/.jupyter/jupyter_lab_config.py -O "https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle/-/raw/master/docker/jupyter_lab_config.py"
RUN curl -fsSL -v -o ~/.jupyter/nbconfig/notebook.json -O "https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle/-/raw/master/docker/notebook.json"

# Fetch datasets and notebooks
# - datasets
RUN mkdir /data && \
    fid  install_datasets --quiet --install_dir /data
# - notebooks
RUN mkdir /notebooks/ && \
    fid install_notebooks --quiet --install_dir /notebooks

VOLUME /notebooks
WORKDIR /notebooks

EXPOSE 8888

ENV PYTHONPATH=~/notebooks/fidle-master/:$PYTHONPATH
ENV SHELL=/bin/bash
ENV FIDLE_DATASETS_DIR=/data/datasets-fidle

CMD ["jupyter", "lab"]
