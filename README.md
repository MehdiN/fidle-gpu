# Repository to install the Fidle docker environment with GPU capabilities

[Link to the Fidle course](https://gricad-gitlab.univ-grenoble-alpes.fr/talks/fidle/-/wikis/Fidle%20%C3%A0%20distance/Pr%C3%A9sentation)

Dependencies:
 - Docker v19.03+ 
 - Docker Compose v1.28.0+ (Optional)
 - [Nvidia Docker Toolkit](https://github.com/NVIDIA/nvidia-docker)
 - Nvidia Linux Drivers >= 418.81.07

## Build the docker



```
git clone https://gitlab.inria.fr/nmehdi/fidle-gpu.git
cd fidle-gpu
docker build -t fidle-gpu -f Dockerfile .

```


## Create the container

```
docker create -p 8888:8888 --gpus all --ipc=host --name Fidle fidle-gpu
```

## Start the container

```
docker start -i Fidle
```

- Copy the URL printed on screen beginning with `http://127.0.0.1:8888/?token`...
- Open a Web Browser and paste the URL
- Jupyterlab for Fidle should be there !

To stop the container:

- In Jupyter lab menu : "File"/"Shutdown"
- Control-C to stop this server and shutdown all kernels

## WIP:

- [ ] Use docker compose to build the image and handle the container
- [ ] Allow to pass arguments to jupyter lab
- [ ] Include host user ID to copy files in host folders

