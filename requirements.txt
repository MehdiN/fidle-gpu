numpy
matplotlib
pandas
scipy
jupyterlab
scikit-learn
scikit-image
fidle
plotly
barviz
tensorflow
torch
torchvision
# add optional packages below
# seaborn
